//
//  Plane.swift
//  ArcTest02
//
//  Created by student on 21.11.17.
//  Copyright © 2017 student. All rights reserved.
//

import Foundation
import SceneKit
import ARKit

class Plane: SCNNode{
    
    // MARK: - Properties
    
    var anchor: ARPlaneAnchor
    
    // MARK: - Initialization
    
    init(_ anchor: ARPlaneAnchor) {
        self.anchor = anchor
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - ARKit
    
    func update(_ anchor: ARPlaneAnchor) {
        self.anchor = anchor
    }
    
}
