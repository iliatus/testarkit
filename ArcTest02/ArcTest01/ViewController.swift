//
//  ViewController.swift
//  ArcTest01
//
//  Created by student on 14.11.17.
//  Copyright © 2017 student. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var btnRestart: UIButton!
    
    var nodeModel:SCNNode!
    let nodeName = "Wind"
    //let nodeName = "Admiral3751"
    
    var stopedDetection: Bool = false
    var oppp: CGFloat = 0.25
    var d: Float = 0
    var iter: Float = 1
    
    let standardConfiguration: ARWorldTrackingConfiguration = {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        return configuration
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let rect = CGRect(x: 10, y: 10, width: 100, height: 100)
        //let myView = UIView(frame: rect)
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        //sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        sceneView.antialiasingMode = .multisampling4X
        
        sceneView.autoenablesDefaultLighting = true; //
        sceneView.isUserInteractionEnabled = true;
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        //rotation
        let RotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(ViewController.Rotation))
        self.view.addGestureRecognizer(RotationGesture)
        
        // Create a new scene
        let scene = SCNScene()
        
        //let scene = SCNScene(named: "art.scnassets/ship.scn")!
       
        //let cubeNode = SCNNode(geometry: SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0))
        //cubeNode.position = SCNVector3(0, 0, -0.2) // SceneKit/AR coordinates are in meters
        //sceneView.scene.rootNode.addChildNode(cubeNode)
        
        // Set the scene to the view
        sceneView.scene = scene
        
        let modelScene = SCNScene(named:
            //"art.scnassets/Admiral3751.dae")!
            "art.scnassets/Wind.dae")!
        
        nodeModel =  modelScene.rootNode.childNode(
            withName: nodeName, recursively: true)!
        
        /*let material = SCNMaterial()
        material.diffuse.contents = UIImage(named: "art.scnassets/Admiral_map.png")
        nodeModel.geometry?.materials = [material]*/
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        //let configuration = ARWorldTrackingConfiguration()
        let configuration: ARWorldTrackingConfiguration = standardConfiguration.copy() as! ARWorldTrackingConfiguration
        
        configuration.planeDetection = .horizontal  //////
        
        sceneView.session.run(configuration)        // Run the view's session
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // This visualization covers only detected planes.
        /*if (stopedDetection) {return}*/
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        // Create a SceneKit plane to visualize the node using its position and extent.
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeNode = SCNNode(geometry: plane)
        planeNode.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z)
        
        // SCNPlanes are vertically oriented in their local coordinate space.
        // Rotate it to match the horizontal orientation of the ARPlaneAnchor.
        
        //planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
        planeNode.eulerAngles.x = -.pi / 2
        // Make the plane visualization semitransparent to clearly show real-world placement.
        planeNode.opacity = 0.25
        
        // Add some object
        /*let cubeNode = SCNNode(geometry: SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0))
        cubeNode.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z) // SceneKit/AR coordinates are in meters
        cubeNode.opacity = 1
        planeNode.addChildNode(cubeNode)*/
        
        // ARKit owns the node corresponding to the anchor, so make the plane a child node.
        node.addChildNode(planeNode)
        
        DispatchQueue.main.async {
            let modelClone = self.nodeModel.clone()
            modelClone.position = SCNVector3Zero
            
            // Create a LookAt constraint, point at the cameras POV
            /*let constraint = SCNLookAtConstraint(target: self.sceneView.pointOfView)
            
            // Keep the rotation on the horizon
            constraint.isGimbalLockEnabled = true
            
            // Slow the constraint down a bit
            constraint.influenceFactor = 0.01
            
            // Finally add the constraint to the node
            node.constraints = [constraint]
            modelClone.constraints = [constraint]
            */
            modelClone.scale = SCNVector3(1, 1, 1)
    
            // Add model as a child of the node
            node.addChildNode(modelClone)
        }
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
     // Update content only for plane anchors and nodes matching the setup created in `renderer(_:didAdd:for:)`.
        
     //if (stopedDetection) {return}
        
     guard let planeAnchor = anchor as?  ARPlaneAnchor,
     let planeNode = node.childNodes.first,
     let plane = planeNode.geometry as? SCNPlane
     else { return }
     
     // Plane estimation may shift the center of a plane relative to its anchor's transform.
        
     planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z)
        
     /*
     Plane estimation may extend the size of the plane, or combine previously detected
     planes into a larger one. In the latter case, `ARSCNView` automatically deletes the
     corresponding node for one plane, then calls this method to update the size of
     the remaining plane.
     */
 
        /*if stopedDetection{
            let cubeNode = SCNNode(geometry: SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0))
            cubeNode.position = SCNVector3Make(planeAnchor.center.x, planeAnchor.center.y + d, planeAnchor.center.z)
            cubeNode.opacity = 5
            planeNode.addChildNode(modelClone)
            //stopedDetection = !stopedDetection
        }*/
        
     plane.width = CGFloat(planeAnchor.extent.x)
     plane.height = CGFloat(planeAnchor.extent.z)
    }
    
    /*
     Called when SceneKit node corresponding to a removed
     AR anchor has been removed from the scene.
     */    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        // ...
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        super.touchesBegan(touches, with: event)
        
        //let touch: UITouch = touches.first as! UITouch
        //stopedDetection = !stopedDetection
        
        // Get our existing session configuration
        let configuration = self.sceneView.session.configuration as! ARWorldTrackingConfiguration;
        // Turn off future plane detection and updating
        configuration.planeDetection = []
        // Re-run the session for the changes to take effect
        self.sceneView.session.run(configuration)
        
        // add models
        let location = touches.first!.location(in: sceneView)
        
        var hitTestOptions = [SCNHitTestOption: Any]()
        hitTestOptions[SCNHitTestOption.boundingBoxOnly] = true
        
        // remove hit node
        /*let hitResults: [SCNHitTestResult]  =
            sceneView.hitTest(location, options: hitTestOptions)
        
        if let hit = hitResults.first {
            if let node = getParent(hit.node) {
                node.removeFromParentNode()
                return
            }
        }*/
        
        let hitResultsFeaturePoints: [ARHitTestResult] =
            sceneView.hitTest(location, types: .featurePoint)
        if let hit = hitResultsFeaturePoints.first {
            sceneView.session.add(anchor: ARAnchor(transform: hit.worldTransform))
        }
    }
    
    func getParent(_ nodeFound: SCNNode?) -> SCNNode? {
        if let node = nodeFound {
            if node.name == nodeName {
                return node
            } else if let parent = node.parent {
                return getParent(parent)
            }
        }
        return nil
    }
    
    /*@IBAction func rotatePiece(_ gestureRecognizer : UIRotationGestureRecognizer) {   // Move the anchor point of the view's layer to the center of the user's two fingers. This creates a more natural looking rotation.
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            gestureRecognizer.view?.transform = gestureRecognizer.view!.transform.rotated(by: gestureRecognizer.rotation)
            gestureRecognizer.rotation = 0
        }}*/
    
    @objc func Rotation(sender: UIRotationGestureRecognizer)
    {
        //sender.sceneView.scene.rootNode.rotation = SCNVector4Make(0, 0, 1, Float(-Double.pi*0.75))
        //sender.rotation = 0
        guard sender.view != nil else { return }
        
        if sender.state == .began || sender.state == .changed {
            self.sceneView.scene.rootNode.rotation = SCNVector4Make(0, 0, 1, Float(sender.rotation))
            //sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        }
    }
    
    @IBAction func btnRestartClick(_ sender: UIButton) {
        resetTracking()
        /*if stopedDetection{
            resetTracking()
            stopedDetection = !stopedDetection
        }
        else {
            let configuration = self.sceneView.session.configuration as! ARWorldTrackingConfiguration;
            // Turn off future plane detection and updating
            configuration.planeDetection = []
            // Re-run the session for the changes to take effect
            self.sceneView.session.run(configuration)
            
            // remove hit node
            /*let hitResults: [SCNHitTestResult]  =
             sceneView.hitTest(location, options: hitTestOptions)
             
             if let hit = hitResults.first {
             if let node = getParent(hit.node) {
             node.removeFromParentNode()
             return
             }
             }*/
            
            let hitResultsFeaturePoints: [ARHitTestResult] =
                sceneView.hitTest(location, types: .featurePoint)
            if let hit = hitResultsFeaturePoints.first {
                sceneView.session.add(anchor: ARAnchor(transform: hit.worldTransform))
            }
            
            
            stopedDetection = !stopedDetection
        }*/
    }
    
    func resetTracking() {
        self.sceneView.session.run(standardConfiguration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>?, with event: UIEvent?) {
        // Don't forget to add "?" after Set<UITouch>
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
